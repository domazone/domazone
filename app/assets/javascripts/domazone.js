window.Domazone = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    window.router =  new Domazone.Routers.Domains()
  }
};

$(document).ready(function(){
  Domazone.initialize();
  Backbone.history.start({pushState: true});
});
