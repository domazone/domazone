Domazone.Views.DomainsIndex = Backbone.View.extend({
  el: '#container',

  template: JST['domains/index'],

  initialize: function() {
    this.render()
  },

  render: function() {
    this.$el.html('<div class="loader-circle">Loading...</div>')
    this.$el.html(this.template())
  }
});
