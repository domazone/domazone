class Dashboard::BaseController < ApplicationController
  before_action :authenticate_user!

  def index
    @domains = Domain.all
  end
end
