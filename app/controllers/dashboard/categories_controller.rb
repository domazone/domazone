class Dashboard::CategoriesController <  Dashboard::BaseController
  before_action :get_category, only: %i(edit update delete)
  before_action :category, only: %i(new create)

  def index
    @categories = Category.all
  end

  def create
    @category.assign_attributes(category_params)

    if @category.save
      flash.now[:success] = 'Successfylly created a category.'
      redirect_to dashboard_categories_path
    else
      render :new
    end
  end

  def update
    @category.assign_attributes(category_params)

    if @category.save
      flash.now[:success] = 'Successfylly created a category.'
      redirect_to dashboard_categories_path
    else
      render :edit
    end
  end

  private

  def category
    @category = Category.new
  end

  def get_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name, :featured)
  end
end
