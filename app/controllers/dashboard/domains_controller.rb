class Dashboard::DomainsController <  Dashboard::BaseController
  before_action :get_domain, only: %i(edit update delete)
  before_action :domain, only: %i(new create)

  def create
    @domain.assign_attributes(domain_params)

    if @domain.save
      flash.now[:success] = 'Successfylly created a domain.'
      redirect_to dashboard_domains_path
    else
      render :new
    end
  end

  def update
    @domain.assign_attributes(domain_params)

    if @domain.save
      flash.now[:success] = 'Successfylly updated a domain.'
      redirect_to dashboard_domains_path
    else
      render :edit
    end
  end

  private

  def domain
    @domain = Domain.new
  end

  def get_domain
    @domain = Domain.find(params[:id])
  end

  def domain_params
    params.require(:domain).permit(:name, :status, :logo, category_ids: [])
  end
end
