class Dashboard::OffersController < Dashboard::BaseController
  before_action :get_offer, only: %i(update show)

  def index
    @offers = Offer.all
  end

  def create
  end

  def update
    @offer.assign_attributes(offer_params)

    if @offer.save
      flash.now[:success] = 'Successfylly updated an offer.'
      redirect_to dashboard_offers_path
    else
      render :show
    end
  end

  private

  def offer_params
    params.require(:offer).permit(:status)
  end

  def get_offer
    @offer = Offer.find(params[:id])
  end
end
