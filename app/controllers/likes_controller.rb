class LikesController < ApplicationController
  def update
    @domain = Domain.find(params[:id])
    @domain.increment(:likes, by = 1)

    if @domain.save
      redirect_to :back, flash: { notice: "Thanks, how about an offer as well" }
    else
      redirect_to :back, flash: { alert: 'Something went wrong, lets try again' }
    end
  end
end
