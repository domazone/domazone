class OffersController < ApplicationController
  before_action :get_domain

  def create
    @offer.assign_attributes(offer_params)

    if @offer.save
      redirect_to :root, flash: { notice: 'Thanks, we have received your offer, we will get back to you as soon as possible.' }
    else
      render :new
    end
  end

  private

  def get_domain
    @domain = Domain.find(params[:domain_id])
    @offer = @domain.offers.new
  end

  def offer_params
    params.require(:offer).permit(:name, :email, :company, :amount, :message)
  end
end
