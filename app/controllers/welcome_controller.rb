class WelcomeController < ApplicationController
  def index
    @categories = Category.all
    @domains = Domain.where(status: 'active')
                     .filter(params.slice(:starts_with, :category))
                     .page(params[:page])
  end
end
