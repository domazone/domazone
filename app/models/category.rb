class Category < ActiveRecord::Base
  has_and_belongs_to_many :domains
  validates :name, presence: true
end
