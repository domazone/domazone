class Domain < ActiveRecord::Base
  include Filterable
  extend FriendlyId

  friendly_id :name, use: [:slugged, :finders]

  has_many :offers
  has_and_belongs_to_many :categories
  accepts_nested_attributes_for :categories

  enum status: %w(active inactive pending sold)

  default_scope { order('created_at ASC') }
  scope :starts_with, -> (alphabet) { where("name like ?", "#{ alphabet }%")}

  has_attached_file :logo, styles: { medium: "400x230>", thumb: "300x150>" }, default_url: "http://placehold.it/400x230"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/

  validates :name, :logo, :status, presence: true

  before_save :normalize!

  def self.category(name)
    Category.find_by(name: name).domains
  end

  def normalize!
    self.name = self.name.downcase
  end
end
