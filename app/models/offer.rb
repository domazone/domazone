class Offer < ActiveRecord::Base
  belongs_to :domain, counter_cache: true
  enum status: %w(contact_buyer pending_transaction accepted)
  default_scope { order('created_at ASC') }

  validates :name, :email, :amount, :message , presence: true
end
