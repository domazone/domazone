role :app,          %w{104.236.97.190}
role :web,          %w{104.236.97.190}

set :user,          'root'
set :branch,        'master'
set :location,      '104.236.97.190'
set :rails_env,     'production'
set :migrate_env,   :production
set :ssh_options,   { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
