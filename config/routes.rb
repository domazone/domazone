Rails.application.routes.draw do
  devise_for :users, skip: %i(passwords registrations)
  root 'welcome#index'
  resources :likes , only: %i(update)

  namespace :dashboard do
    root 'base#index'
    resources :domains
    resources :offers
    resources :categories
  end

  resources :domains do
    resources :offers, only: %i(new create)
  end
end
