class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :name
      t.string :email
      t.string :company
      t.float :amount
      t.text :message
      t.integer :status, default: 0
      t.references :domain, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
