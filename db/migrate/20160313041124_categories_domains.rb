class CategoriesDomains < ActiveRecord::Migration
  def change
    create_table :categories_domains, id: false do |t|
      t.belongs_to :domain, index: true
      t.belongs_to :category, index: true
    end
  end
end
