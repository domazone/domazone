class AddOfferAndLikeCounts < ActiveRecord::Migration
  def change
    add_column :domains, :starting_offers, :integer, default: 0
    add_column :domains, :likes, :integer, default: 0
  end
end
