class AddOffersCountOnDomain < ActiveRecord::Migration
  def change
    add_column :domains, :offers_count, :integer
  end
end
