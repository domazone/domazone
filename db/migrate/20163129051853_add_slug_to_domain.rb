class AddSlugToDomain < ActiveRecord::Migration
  def change
    add_column :domains, :slug, :string, unique: true, index: true
  end
end
