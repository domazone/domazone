class AddLogoToDomain < ActiveRecord::Migration
  def up
    add_attachment :domains, :logo
  end

  def down
    remove_attachment :domains, :logo
  end
end
